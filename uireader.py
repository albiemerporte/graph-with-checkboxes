# reference https://www.pythonguis.com/tutorials/plotting-pyqtgraph/#customizing-pyqtgraph-plots

from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication
from PyQt6.QtGui import QColor
import pyqtgraph as pg

class Main:
    def __init__(self):
        self.mainui = loadUi('chkgraph.ui')
        self.mainui.show()

        self.plot_widget = pg.PlotWidget()
        self.mainui.vboxLayout.addWidget(self.plot_widget)

        self.mainui.chkApple.stateChanged.connect(self.appletrend)
        self.mainui.chkOrange.stateChanged.connect(self.orangetrend)
        self.mainui.chkBanana.stateChanged.connect(self.bananatrend)

        self.price_apple = [23,22,33,44,22,33,44,55,44,44]
        self.date_apple = list(range(0,10))

        self.price_orange = [23,10,19,44,22,23,26,55,44,44]
        self.date_orange = list(range(0, 10))

        self.price_banana = [21,22,29,40,10,2,1,55,56,59]
        self.date_banana = list(range(0, 10))

        self.orangeline = QColor(255, 165, 0)
        self.yellowline = QColor(255, 255, 0)

        self.apple = self.plot_widget.plot(self.date_apple, self.price_apple, pen='r', symbol='o', name='apple')
        self.orange = self.plot_widget.plot(self.date_orange, self.price_orange, pen=self.orangeline, symbol='o', name='orange')
        self.banana = self.plot_widget.plot(self.date_banana, self.price_banana, pen=self.yellowline, symbol='o', name='banana')

    def appletrend(self):
        if self.mainui.chkApple.isChecked():
            self.apple = self.plot_widget.plot(self.date_apple, self.price_apple, pen='r', symbol='o', name='apple')
        else:
            self.plot_widget.removeItem(self.apple)

    def orangetrend(self):
        if self.mainui.chkOrange.isChecked():
            self.orange = self.plot_widget.plot(self.date_orange, self.price_orange, pen=self.orangeline, symbol='o', name='orange')
        else:
            self.plot_widget.removeItem(self.orange)

    def bananatrend(self):
        if self.mainui.chkBanana.isChecked():
            self.banana = self.plot_widget.plot(self.date_banana, self.price_banana, pen=self.yellowline, symbol='o', name='banana')
        else:
            self.plot_widget.removeItem(self.banana)


if __name__ == '__main__':
    app = QApplication([])
    main = Main()
    app.exec()